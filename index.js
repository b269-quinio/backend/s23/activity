
let trainer = {
    name: "Ash Ketchum",
    age: 10,
    pokemon: ["Pikachu", "Charizard", "Squirtle", "Bulbusaur"],
    friends: {
        hoenn:["May", "Max"],
        kanto:["Brock", "Misty"]
    },
    talk: function() {
        console.log('Pikachu! I choose you!')
    }
}

console.log(trainer);
console.log("Result of dot notation: ");
console.log(trainer.name);
console.log("Result of square bracket notation: ");
console.log(trainer['pokemon']);
console.log("Result of talk method");
trainer.talk();


// Real World Application Of Objects
/*
    - Scenario
        1. We would like to create a game that would have several pokemon interact with each other
        2. Every pokemon would have the same set of stats, properties and functions
*/


// Creates new instances of the "Pokemon" object each with their unique properties
let geodude = new Pokemon('Geodude', 8, 16, 8);
let pikachu = new Pokemon("Pikachu", 12, 24, 12);
let mewtwo = new Pokemon ('Mewtwo', 100, 200, 100);


console.log(pikachu);
console.log(geodude);
console.log(mewtwo);


function Pokemon(name, level, health, attack) {

    // Properties
    this.name = name;
    this.level = level;
    this.health = health;
    this.attack = attack;

    //Methods
    this.tackle = function(target) {
        let targetPokemonHealth = target.health - this.attack

        console.log(this.name + ' tackled ' + target.name);
        console.log(target.name + "'s health is now reduced to " + targetPokemonHealth);
        
        let pikachu2 = new Pokemon(target.name, target.level, targetPokemonHealth, target.attack)
        console.log(pikachu2);

        if(targetPokemonHealth <= 0){
            return this.faint;
        }
    };




    this.faint = function(deplete){
        let depletePokemonHealth = deplete.health - this.attack

        console.log(this.name + ' tackled ' + deplete.name);
        console.log(deplete.name + "'s health is now reduced to " + depletePokemonHealth);
        console.log(deplete.name + ' fainted.');

        let geodude2 = new Pokemon(deplete.name, deplete.level, depletePokemonHealth, deplete.attack)
        console.log(geodude2);

    }

}

// Providing the "geodude" object as an argument to the "pikachu" tackle method will create interaction between the two objects
geodude.tackle(pikachu);
mewtwo.faint(geodude);

